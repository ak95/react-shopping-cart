import React from "react";
import ReactDOM from "react-dom/client";
import Router from "./Router.jsx";
import "../styles/main.scss";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Router />
    <a
      href="https://unsplash.com/photos/QpIayO5KIRE?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"
      className="text-black"
    >
      Attribution
    </a>
  </React.StrictMode>
);
