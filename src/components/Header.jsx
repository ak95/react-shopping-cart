import { NavLink } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHouse, faShop } from "@fortawesome/free-solid-svg-icons";
import "../styles/Header.scss";
import logo from "../assets/logo.png";

function Header() {
  const showText = useMediaQuery({ minWidth: 1000 });

  return (
    <header className="mx-auto d-flex w-100 px-4">
      <div className="mb-3 d-flex flex-grow-1 justify-content-between gap-4">
        <NavLink
          to="/"
          className="d-flex justify-content-center align-items-center logo gap-4"
          aria-label="go to home page"
        >
          <img src={logo} alt="dee's fish house logo" />
          {showText ? <h1>dee's fish house</h1> : null}
        </NavLink>
        <nav className="nav nav-masthead justify-content-center float-md-end d-flex align-items-center gap-4">
          <NavLink
            to="/"
            as="button"
            type="button"
            className="btn btn-outline-light px-4"
          >
            <FontAwesomeIcon icon={faHouse} /> Home
          </NavLink>
          <NavLink
            to="shop"
            as="button"
            type="button"
            className="btn btn-outline-light px-4"
          >
            <FontAwesomeIcon icon={faShop} /> Shop
          </NavLink>
        </nav>
      </div>
    </header>
  );
}

export default Header;
