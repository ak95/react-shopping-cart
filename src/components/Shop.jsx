import { useState, useEffect, useRef } from "react";
import { useMediaQuery } from "react-responsive";
import { useNavigate } from "react-router-dom";
import { renderToStaticMarkup } from "react-dom/server";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBagShopping,
  faCartShopping,
  faPlus,
  faMinus,
} from "@fortawesome/free-solid-svg-icons";
import "../styles/Shop.scss";

function Shop() {
  const [items, setItems] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [scroll, setScroll] = useState(0);
  const navigate = useNavigate();
  const freshCatchPrice = useRef(
    Number.parseFloat((Math.random() * 39.99).toFixed(2))
  );
  const isDesktop = useMediaQuery({ minWidth: 1000 });
  const menu = [
    {
      name: `Oysters (1 dozen)`,
      price: 19.99,
    },
    {
      name: `Beer-battered fish and chips`,
      price: 14.99,
    },
    {
      name: `Baja-style fish tacos (3)`,
      price: 14.99,
    },
    {
      name: `Local Sandabs with Tartar sauce`,
      price: 19.99,
    },
    {
      name: `Seared Ahi Tuna over salad and rice with Sweet Chili sauce`,
      price: 24.99,
    },
    {
      name: `Cedar plank smoked Salmon with rice and Terriyaki sauce`,
      price: 29.99,
    },
    {
      name: `Halibut with scalloped potatoes and green beans`,
      price: 19.99,
    },
    {
      name: `Giant Alaskan Crab meal`,
      price: 29.99,
    },
    {
      name: `Chef's Special`,
      price: 24.99,
    },
    {
      name: `Catch of the Day`,
      price: freshCatchPrice.current,
    },
  ];

  const MenuItemContainer = ({ name, price }) => {
    function clicked(event) {
      // get name of clicked item
      const name = event.target.firstChild.textContent;
      // check if its already been clicked
      let existingItem, index;
      for (let i = 0; i < items.length; i++) {
        // if yes, copy item
        if (items[i].name === name) {
          existingItem = {
            name: items[i].name,
            price: items[i].price,
            clicks: items[i].clicks + 1,
          };
          index = i;
          break;
        }
      }
      // then send to new array that is then sent to state
      if (existingItem) {
        let newArray = items;
        newArray[index] = existingItem;
        setItems(newArray);
        setRefresh(!refresh);
      }
      // otherwise if the clicked check failed
      else {
        // find item in menu array
        const menuItem = menu.find((item) => item.name === name);
        // make new item for cart with clicks set to 0
        const newItem = {
          name: menuItem.name,
          price: menuItem.price,
          clicks: 1,
        };
        // add item to items array
        setItems([...items, newItem]);
      }
    }

    return (
      <button
        type="button"
        className="btn border border-1 border-outline border-secondary p-4 d-flex justify-content-between align-items-center w-100 flex-grow-1 gap-5"
        onClick={clicked}
      >
        <div className="fs-6">{name}</div>
        <div className="fs-6">{`$${price}`}</div>
      </button>
    );
  };

  // if mobile, menu is displayed in one div otherwise in two

  const Mobile = () => {
    const output = menu.map((menuItem, index) => (
      <MenuItemContainer
        name={menuItem.name}
        price={menuItem.price}
        key={index}
      />
    ));
    return (
      <>
        <div className="d-flex flex-column p-4 border border-secondary gap-4 overflow-scroll border-end-0">
          {output}
        </div>
      </>
    );
  };

  const Desktop = () => {
    let left, right;
    const middle = Math.floor(menu.length / 2);
    [left, right] = [menu.slice(0, middle), menu.slice(middle, menu.length)];
    left = left.map((menuItem, index) => (
      <MenuItemContainer
        name={menuItem.name}
        price={menuItem.price}
        key={index}
      />
    ));
    right = right.map((menuItem, index) => (
      <MenuItemContainer
        name={menuItem.name}
        price={menuItem.price}
        key={index + (left.length - 1)}
      />
    ));

    return (
      <>
        <div className="d-inline-flex p-4 border border-secondary gap-4 overflow-scrolls border-end-0 justify-content-evenly">
          <div className="d-inline-flex flex-column gap-4">{left}</div>
          <div className="d-inline-flex flex-column gap-4">{right}</div>
        </div>
      </>
    );
  };

  const CartItemContainer = ({ name, price, clicks }) => {
    function handleChange(event) {
      // get name of associated item in cart
      const itemName =
        event.currentTarget.parentNode.parentNode.parentNode.firstChild
          .textContent;

      // find associated item
      let item, index, delbool;
      for (let i = 0; i < items.length; i++) {
        // if found
        if (items[i].name === itemName) {
          item = items[i];
          index = i;
          break;
        }
      }

      let newArray = items;

      function setStates() {
        setItems(newArray);
        setRefresh(!refresh);
        if (scroll != document.querySelector(".cart").scrollTop)
          setScroll(document.querySelector(".cart").scrollTop);
        return;
      }

      if (event.currentTarget.id === "quantity") {
        function finish() {
          if (event.currentTarget.value != item.value) {
            if (event.currentTarget.value < 0) delbool = true;
            if (delbool) {
              newArray.splice(index, 1);
            } else {
              item.clicks = event.currentTarget.value;
              newArray[index] = item;
            }
            setStates();
          }
        }
        if (event.key === "Enter" || event.key === "Tab") {
          finish();
        }
        if (event.key === "Backspace") {
          event.currentTarget.value = event.currentTarget.value.slice(0, -1);
          return;
        }
      }

      if (
        event.currentTarget.id === "plus" ||
        event.currentTarget.id === "minus"
      ) {
        // handle changes - plus
        if (event.currentTarget.id === "plus") {
          item.clicks += 1;
        }

        // handle changes - minus
        if (event.currentTarget.id === "minus") {
          item.clicks -= 1;
          if (item.clicks < 1) {
            delbool = true;
          }
        }

        // assign to new array, then to state
        let newArray = items;
        if (delbool) {
          newArray.splice(index, 1);
        } else {
          newArray[index] = item;
        }
        setStates();
      }
    }
    return (
      <div className="border border-secondary rounded p-4 d-flex justify-content-between align-items-center gap-4">
        <div className="fs-6">{name}</div>
        <div className="d-flex gap-4 align-items-center justify-content-end">
          <div className="fs-6">{`$${price}`}</div>
          <div className="d-flex gap-2 align-items-equal align-items-center">
            <button
              type="button"
              className="btn p-2"
              id="plus"
              onClick={handleChange}
            >
              <FontAwesomeIcon icon={faPlus} />
            </button>
            <input
              type="text"
              className="form-control fs-6 p-0"
              aria-label="item quantity"
              id="quantity"
              onKeyDown={handleChange}
              size="3"
              placeholder={clicks}
            />
            <button
              type="button"
              className="btn p-2"
              id="minus"
              onClick={handleChange}
            >
              <FontAwesomeIcon icon={faMinus} />
            </button>
          </div>
        </div>
      </div>
    );
  };

  const Cart = () => {
    const output = items.map((item, index) => (
      <CartItemContainer
        name={item.name}
        price={item.price}
        clicks={item.clicks}
        key={index}
      />
    ));

    return (
      <div className="d-flex flex-column p-4 border border-secondary gap-2 overflow-scroll cart">
        {output}
      </div>
    );
  };

  // runs on first load
  useEffect(() => {
    // sets up additional cart output div on navbar
    const nav = document.querySelector("nav");
    const cart = document.createElement("div");
    cart.className =
      "border border-1 border-color-white rounded-2 text-white px-4 cart-icon";
    cart.innerHTML = renderToStaticMarkup(
      <>
        <FontAwesomeIcon icon={faCartShopping} /> Cart ({items.length})
      </>
    );
    nav.appendChild(cart);

    // sets up dummy checkout button on navbar
    const checkout = document.createElement("button");
    checkout.className = "btn btn-success px-4 checkout";
    checkout.onclick = () => {
      alert(`Thank you for shopping at Dee's Fish House! Please come again!`);
      navigate("/");
    };
    checkout.innerHTML = renderToStaticMarkup(
      <>
        <FontAwesomeIcon icon={faBagShopping} /> Check Out
      </>
    );
    nav.appendChild(checkout);

    return () => {
      nav.removeChild(cart);
      nav.removeChild(checkout);
    };
  }, []);

  // updates items in cart when items updates
  useEffect(() => {
    let finalCount = 0;
    items.forEach((item) => {
      item.clicks > 0
        ? (finalCount = finalCount * 1 + item.clicks * 1)
        : finalCount++;
    });
    document.querySelector(".cart-icon").innerHTML = renderToStaticMarkup(
      <>
        <FontAwesomeIcon icon={faCartShopping} /> Cart ({finalCount})
      </>
    );
    document.querySelector(".cart").scrollTop = scroll;
  }, [items, refresh, scroll]);

  return (
    <div className="d-flex flex-row justify-content-between p-4 shop flex-grow-1 overflow-hidden">
      {isDesktop ? Desktop() : Mobile()}
      <Cart />
    </div>
  );
}

export default Shop;
