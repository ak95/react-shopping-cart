import { createBrowserRouter, RouterProvider, Outlet } from "react-router-dom";
import ErrorPage from "./ErrorPage.jsx";
import Home from "./Home.jsx";
import Shop from "./Shop.jsx";
import Header from "./Header.jsx";

const Layout = () => (
  <div className="d-flex w-100 mx-auto flex-column">
    <Header />
    <div className="card flex-grow-1 overflow-hidden">
      <Outlet />
    </div>
  </div>
);

const Router = () => {
  const router = createBrowserRouter([
    {
      element: <Layout />,
      children: [
        {
          path: "/",
          element: <Home />,
          errorElement: <ErrorPage />,
        },
        {
          path: "/shop",
          element: <Shop />,
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
};

export default Router;
