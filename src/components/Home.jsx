import { useMediaQuery } from "react-responsive";
import "../styles/Home.scss";

function Home() {
  const Default = () => (
    <div className="d-flex flex-column flex-grow-1 justify-content-around px-4">
      <div className="d-flex flex-column align-items-center text-center">
        <h2 className="fs-1">
          Welcome to <span className="fw-bold"> dee's</span>
        </h2>
        <h3 className="fs-5">Home of the Local Fresh Catch Special</h3>
      </div>
      <h4 className="fs-6 text-center">
        Here at Dee's, we pride ourselves on our freshness. From our daily local
        catch to our award-winning nuts, all ingredients are sourced right on
        site, and cooked to perfection by our Michelin-certified chefs. This
        isn't just any fish restaurant! We strive to provide you a gourmet
        experience like no other. Come down to Dee's today to see what all the
        hype is about!
      </h4>
    </div>
  );

  const isDesktop = useMediaQuery({ minWidth: 1000 });

  const Desktop = () => (
    <>
      <div className="d-flex flex-column justify-content-around gap-4">
        <div
          className="d-flex flex-grow-1 biglogo"
          alt="Dee's Fish House Logo"
        />
        <div className="d-flex flex-grow-1 panfrying">
          <a
            href="https://www.freepik.com/free-photo/side-view-mushroom-frying-with-gas-stove-fire-human-hand-pan_7724732.htm"
            className="text-black"
          >
            Attribution
          </a>
        </div>
      </div>
      <Default />
      <div className="d-flex fish">
        <a
          href="https://unsplash.com/photos/aISWW34SG88?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText"
          className="text-black"
        >
          Attribution
        </a>
      </div>
    </>
  );

  return (
    <div className="d-flex flex-row flex-grow-1 justify-content-between home">
      {isDesktop ? Desktop() : Default()}
    </div>
  );
}

export default Home;
