import { Link } from "react-router-dom";

const ErrorPage = () => {
  return (
    <div>
      <h1>Invalid link!</h1>
      <Link to="/">Click here to return to the home page</Link>
    </div>
  );
};

export default ErrorPage;
